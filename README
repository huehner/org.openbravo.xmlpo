This application is base on the openbravo2po tool created by:

HeadIssue (http://www.headissue.com/) Openbravo XML to PO tools
Author: Jens Wilke, www.headissue.com

It has been adapted by Openbravo to provide additional functionality. If you have any questions
regarding the use or functionality in this application please contact me at: 
	phillip.heenan@openbravo.com
	or Skype: phillip.heenan

This application can now be run and built through the ANT tool using the ant command. For 
further details on the targets available please refer to the build.xml file.

Further documentation to support this application will shortly be available on the Openbravo
wiki at http://wiki.openbravo.com/wiki/index.php/Openbravo2po

FUNCTION: XML2PO Transformation
	Transform an XML file exported from Openbravo into a .po file

	COMMAND: 
		ant runXML2PO 
			-DinpFold=[path to folder] 	
			-DoutFold=[path to folder] 	
			-DmsgStr=[yes/no] 					
			-Dfile="[space separated listing of file to merge]"


FUNCTION: PO2XML Transformation
	Convert .po files into XML files suitable for importing into Openbravo

	COMMAND:
		ant runPO2XML 
			-DinpFold=[path to folder] 
			-DoutFold=[path to folder] 
		
FUNCTION: POMerge Utility
	Merge multiple .po files into a single file and highlight conflicts.

	COMMAND:
		ant runPOMerge 
			-Dfile="[space separated listing of file to merge]"
		
FUNCTION: TemplateMerge
	Using a template file and a translation file synchronise the translation file with the template file.
	Rows from the template file that are not in the translation file will be added.
	Rows from the translation file that are not in the template file will be removed.
	Rows in both files will take the 'Original' string from the template file and the 'Value' string from the translation file.

	COMMAND:
		ant runTemplateMerge
			-DinpTempFold=[path to folder] = this is the folder containing template files 
			-DinpBaseFold=[path to folder] = this is the folder containing the translation files
			-DoutFold=[path to folder] = the folder where the merged files are to be output to

LOGGING:
	A log configuration file is provided and this will output useful information to the 
	XMLPOTransform top level folder. This destination can be changed in the log4j.properties 
	file located in the src folder.
	
UNIT TESTING:
	Unit tests are included which validate the construct of each object and the relevant files
	output by the application. These can be run using the 'ant test' command or executing specific
	tests within the eclipse IDE.
	

