/* 
 * Copyright (C) 2001-2008 Openbravo SL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.openbravo.xmlpo.objects.FileObject;
import org.openbravo.xmlpo.objects.Message;
import org.openbravo.xmlpo.objects.POFileObject;
import org.openbravo.xmlpo.objects.RowObject;
import org.openbravo.xmlpo.objects.ValueType;


public class POFileBuilder {
  
  private static final Logger log4j = Logger.getLogger(POFileBuilder.class);
  
  private FileObject fileObject;
  private Collection<Message> messages = new ArrayList<Message>();
  private ValueType valueType;
  private String contentHeader = "Header not created.";
  private String contentMsgs = "No content found.";
  private String fileName;
  private boolean ignoreTrl = false;
  private boolean mergeFile = false;
  
  public POFileBuilder() {}
  
  public POFileBuilder(boolean isMerge) {
    mergeFile = isMerge;
  }
  
  public POFileBuilder(FileObject fileObj) {
    fileObject = fileObj;
  }
  
  public void setIgnoreNonTrl(boolean ignoreMatchingMsgStr) { ignoreTrl = ignoreMatchingMsgStr; }
  
  public String generateFileContent(ValueType myValueType) {
    setValueType(myValueType);
    setFileName(fileObject.getTableName(), this.valueType.getName(), fileObject.getLanguage());
    String result = new String();
    
    setHeader(getFileName(), fileObject.getTableName(), this.valueType.getName(), fileObject.getLanguage(), fileObject.getVersion(), this.valueType.getIntValue());
    addMessages();
    setContentString();
    
    result = contentHeader + contentMsgs;
    
    return result;
  }
  
  public String generateFileContent(POFileObject poFileObj, HashMap<String, Message> msgs) {
    setValueType(new ValueType(1, poFileObj.getColumnName()));
    setFileName(poFileObj.getTableName(), valueType.getName(), poFileObj.getLanguage());
    String result = new String();
    
    setHeader(poFileObj.getFileName(), poFileObj.getTableName(), valueType.getName(), poFileObj.getLanguage(), poFileObj.getVersion(), valueType.getIntValue());
    addMessages(msgs);
    setContentString();
    
    result = contentHeader + contentMsgs;
    
    return result;
  }
  
  public Collection<Message> getMessages() { return messages; }
  
  public void addMessages() {
    for (Iterator<RowObject> iterator = fileObject.getRowObjects().iterator(); iterator.hasNext();) {
      RowObject row = (RowObject) iterator.next();
      Message msg = buildMessage(row);
      addMessage(msg);
    }
  }
  
  public void addMessages(HashMap<String, Message> msgs) {
    List<Message> listing = new ArrayList<Message>(msgs.values());
    log4j.debug("LISTING SIZE: " + listing.size());
    Collections.sort(listing, new Message());
    for (Iterator<Message> iterator = listing.iterator(); iterator.hasNext();) {
      Message message = (Message) iterator.next();
      addMessage(buildMessage(message));
    }
  }
  
  public boolean addMessage(Message msg) {
    boolean msgRepeated = false;
    if (messages.size()> 0) {
      for (Iterator<Message> iterator = messages.iterator(); iterator.hasNext();) {
        Message message = (Message) iterator.next();
        if (msg.getMessageId().equals(message.getMessageId()) &&
            msg.getMessageString().equals(message.getMessageString())) {
          msgRepeated = true;
          message.setRowIdRow(updateRowId(message.getRowIdRow(), msg.getRowIdRow()));
        }
      }
    }
    if (!msgRepeated) messages.add(msg);
    return msgRepeated;
  }
  
  public String getContentString() { return contentMsgs; }
  public void setContentString() { contentMsgs = buildContentMsgs(); }

  public String getFileName() {
    if (fileName == null) {
      fileName = "";
    }
    return fileName; 
  }
  
  public void setFileName(String tablename, String valueTypeName, String language) { 
    if (mergeFile) {
      fileName = tablename + "_" + valueTypeName + "_" + language + "_MERGED.po";
    } else {
      fileName = tablename + "_" + valueTypeName + "_" + language + ".po";
    }
    log4j.debug("Output filename: " + fileName);
  }
  
  public FileObject getFileObject() { return fileObject; }
  public void setFileObject(FileObject fileObject) { this.fileObject = fileObject; }
  
  public String getHeader() { return contentHeader; }
  public void setHeader(String fileName, String tableName, String valueTypeName, String language, String version, int valueTypeIndex) { 
    contentHeader = buildHeaderString(fileName, tableName, valueTypeName, language, version, valueTypeIndex); 
  }

  public ValueType  getValueType() { return valueType; }
  public void setValueType(ValueType type) { 
    valueType = type; 
    messages = new ArrayList<Message>();
    contentHeader = "";
    contentMsgs = "";
    fileName = "";
  }
  
  private String updateRowId(String origString, String newString) {
    String result = "";

    String origRow = trimRowString(origString);
    String newRow = trimRowString(newString);
    String matchRowTo = origRow;
    if (origRow.indexOf(",") > 0) matchRowTo = origRow.substring(0,origRow.indexOf(",")); 
    String rowId1 = new String(matchRowTo);
    String rowId2 = new String(newRow);
    if (rowId2.compareTo(rowId1) == -1) {
      result = newRow + "," + origRow;
    } else {
      result = origRow + "," + newRow;
    }
    result = rebuildRowString(result);
    return result;
  }
  
  private String rebuildRowString(String string) {
    String result = Message.MESSAGE_FLAG_STRING + Message.LINE_TERMINATOR_STRING;
    
    result = result + Message.COMMENT_FLAG_STRING + string + Message.LINE_TERMINATOR_STRING;
    return result;
  }
  
  private String trimRowString(String string) {
    String result = "";
    
    result = string.replaceFirst(Message.MESSAGE_FLAG_STRING, "");
    result = result.replaceFirst(Message.COMMENT_FLAG_STRING, "");
    result = result.trim();
    return result;
  }
  
  private Message buildMessage(RowObject rowObject) {
    MessageBuilder builder = new MessageBuilder(rowObject, valueType);
    if (ignoreTrl) builder.setIgnoreNonTrl(ignoreTrl);
    Message message = builder.buildMessage();
    return message;
  }
  
  private Message buildMessage(Message message) {
    MessageBuilder builder = new MessageBuilder();
    Message newMessage = builder.buildMessage(message);
    return newMessage;
  }
  
  private String buildContentMsgs() {
    StringBuffer buffer = new StringBuffer();
    MessageBuilder msgBuilder = new MessageBuilder();
    for (Iterator<Message> iterator = messages.iterator(); iterator.hasNext();) {
      Message message = (Message) iterator.next();
      if (!message.getMessageId().equals("") && !message.getMessageId().equals(Message.MSGID_STRING + "\"\n")) {
        buffer.append(msgBuilder.getMessageString(message));
      }
    }
    return buffer.toString();
  }
  
  private String buildHeaderString(String myFileName, String tableName, String valueTypeName, String language, String version, int valueTypeIndex) {
    if (myFileName == null) {
      return "";
    } else if (valueTypeName == null) {
      return "";
    }
    StringBuffer buffer = new StringBuffer();
    buffer.append("msgid \"\"\n");
    buffer.append("msgstr \"\"\n");
    buffer.append("\"Content-Type: text/plain; charset=UTF-8\\n\"\n");
    buffer.append("\"Content-Transfer-Encoding: 8bit\\n\"\n");
    buffer.append("\"Last-Translator: \\n\"\n");
    buffer.append("\"PO-Revision-Date: \\n\"\n");
    buffer.append("\"Project-Id-Version: " + myFileName + "\\n\"\n");
    buffer.append("\"Language-Team:  \\n\"\n");
    buffer.append("\"X-Generator: org.openbravo.xmlpo.xml2po\\n\"\n");
    buffer.append("\"X-Openbravo-Table: " + tableName + "\\n\"\n");
    buffer.append("\"X-Openbravo-Column: " + valueTypeName + "\\n\"\n");
    buffer.append("\"X-Openbravo-Index-Column: " + valueTypeIndex + "\\n\"\n");
    buffer.append("\"X-Openbravo-Language: " + language + "\\n\"\n");
    buffer.append("\"X-Openbravo-Version: " + version + "\\n\"\n");
    buffer.append("\"MIME-Version: 1.0\\n\"\n");
    buffer.append("\n");
    return buffer.toString();
  }
}
