/* 
 * Copyright (C) 2001-2008 Openbravo SL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.utils;

import java.util.Collection;
import java.util.Iterator;

import org.openbravo.xmlpo.objects.Message;
import org.openbravo.xmlpo.objects.RowObject;
import org.openbravo.xmlpo.objects.ValueObject;
import org.openbravo.xmlpo.objects.ValueType;


public class MessageBuilder {
  
  //private static final Logger log4j = Logger.getLogger(MessageBuilder.class);
  
  private Message message;
  private RowObject rowObject;
  private ValueType valueType;
  private String[] comments;
  private String messageIdRow;
  private String messageStrRow;
  private boolean ignoreNonTrl = false;
  private boolean rowTranslated;
  
  public MessageBuilder() {}
  
  public MessageBuilder(RowObject rowObject, ValueType type) {
    this.rowObject = rowObject;
    valueType = type;
    comments = new String[this.rowObject.getValueObjects().size()];
  }

  public void setIgnoreNonTrl(boolean ignoreTrl) { ignoreNonTrl = ignoreTrl; }
  public void setRowTrl(boolean rowTrl) { rowTranslated = rowTrl; }
  
  public Message buildMessage() {
    setRowTrl(rowObject.getTranslation());
    message = new Message();
    cleanCommentsArray();
    
    message.setRowIdRow(buildRowComment(rowObject.getRowId()));
    message.setComments(buildCommentRows());
    //log4j.debug("commentString: " + message.getCommentString());
    message.setMessageId(messageIdRow);
    message.setMessageString(messageStrRow);
    
    return message; 
  }
  
  public Message buildMessage(Message msg) {
    message = new Message();
    
    message.setRowIdRow(buildRowComment(msg.getRowIdRow()));
    message.setComments(msg.getCommentString());
    buildMessageIdRow(msg.getMessageId());
    buildMessageStrRow(msg.getMessageString());
    //log4j.debug("commentString: " + message.getCommentString());
    message.setMessageId(messageIdRow);
    message.setMessageString(messageStrRow);
    
    return message;
  }
  
  public String getMessageString(Message messageObj) {
    StringBuffer buffer = new StringBuffer();
    buffer.append(messageObj.getRowIdRow());
    buffer.append(messageObj.getCommentString());
    buffer.append(messageObj.getMessageId());
    buffer.append(messageObj.getMessageString());
    return buffer.toString();
  }
  
  public String[] getComments() { return comments; }
  public ValueType getValueType() { return valueType; }
  
  public void buildMessageIdRow(String originalValue) { 
	String tempString = StringFormatter.stringEscapeFormat(originalValue, true);
    tempString = tempString.replaceAll("\n", "\"\n\"");
    messageIdRow = Message.MSGID_STRING + parseString(tempString) + "\"\n";
  }
  
  public void buildMessageStrRow(String dataValue) { 
	String tempString = StringFormatter.stringEscapeFormat(dataValue, true);
    tempString = tempString.replaceAll("\n", "\"\n\"");
    messageStrRow = Message.MSGSTR_STRING + parseString(tempString) + "\"\n\n";
  }
  
  private String buildRowComment(String rowId) {
    StringBuffer rowCommentsBuffer = new StringBuffer();
    rowCommentsBuffer.append(Message.MESSAGE_FLAG_STRING + Message.LINE_TERMINATOR_STRING);
    rowCommentsBuffer.append(Message.COMMENT_FLAG_STRING + rowId + Message.LINE_TERMINATOR_STRING);
    return rowCommentsBuffer.toString();
  }
  
  private String buildCommentRows() {
    String result = new String();
    StringBuffer commentsBuffer = new StringBuffer();
    Collection<ValueObject> valueObjects = rowObject.getValueObjects();
    for (Iterator<ValueObject> iterator = valueObjects.iterator(); iterator.hasNext();) {
      ValueObject valueObject = (ValueObject) iterator.next();
      
      String comment = bufferComments(valueObject);
      //log4j.debug("comment buffer: " + comment + "type.intValue: " + valueObject.getType().getIntValue() + ", type.name: " + valueObject.getType().getName());
      comments[valueObject.getType().getIntValue() - 1] = comment;
      
    }
    commentsBuffer.append(Message.COMMENT_FLAG_STRING);
    commentsBuffer.append(Message.ROWTRL_MESSAGE_STRING);
    commentsBuffer.append(formatRowTrl(rowTranslated));
    commentsBuffer.append(Message.LINE_TERMINATOR_STRING);
    for (int i = 0; i < comments.length; i++) {
      commentsBuffer.append(comments[i]);
      //log4j.debug("comment buffer being built: " + commentsBuffer.toString());
    }
    result = commentsBuffer.toString();
    return result;
  }
  
  private String formatRowTrl(boolean value) {
    String result = "Y";
    if (!value) result = "N";
    return result;
  }

  private String bufferComments(ValueObject valueObject) {
    StringBuffer buffer = new StringBuffer();
    if (valueObject.getType().getIntValue() == valueType.getIntValue()) {
      buildMessageIdRow(valueObject.getOriginal());
      if (!ignoreNonTrl || rowTranslated || !valueObject.getOriginal().equals(valueObject.getValue())) {
        buildMessageStrRow(valueObject.getValue());
      } else {
        if (ignoreNonTrl && valueObject.getOriginal().equals(valueObject.getValue())) {
          buildMessageStrRow("");
        }
      }
    }
    String valueName = valueObject.getType().getName();
    String formatName = valueName.substring(0, 1).toUpperCase() + valueName.substring(1);
    buffer.append(Message.COMMENT_FLAG_STRING);
    buffer.append(formatName);
    //buffer.append(valueObject.getType().getName());
    buffer.append(": ");
    String temp = valueObject.getOriginal().replaceAll("\n", "\n# ");
    buffer.append(parseString(temp));
    buffer.append(" [" + rowObject.getRowId());
    buffer.append("]" + Message.LINE_TERMINATOR_STRING);
    return buffer.toString();
  }
  
  private void cleanCommentsArray() {
    for (int i = 0; i < comments.length; i++) {
      comments[i] = "";
    }
  }
  
  private String parseString(String string) {
    String result = "";
    if (!string.equals(null)) result = string;
    return result;
  }
}
