package org.openbravo.xmlpo.utils;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

public class ParserUtils {

	/**
	 * Obtain a new instance of a SAXParserFactory and specify that it will support XML namespaces and validate
	 * the documents as they are parsed. 
	 * 
	 * @return SAXParser
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 */
	public static SAXParser instantiateParser() throws ParserConfigurationException, SAXException {
		SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
		saxParserFactory.setNamespaceAware(true);
		saxParserFactory.setValidating(true);
		SAXParser saxParser = saxParserFactory.newSAXParser();
	    return saxParser;
	}
}
