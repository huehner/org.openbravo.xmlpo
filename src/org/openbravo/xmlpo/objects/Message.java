/* 
 * Copyright headissue GmbH, Jens Wilke, 2007 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.objects;

import java.util.Comparator;

public class Message implements Comparator<Message> {
  
  public static final String MESSAGE_FLAG_STRING = "# RowIds:";
  public static final String MSGID_STRING = "msgid \"";
  public static final String MSGSTR_STRING = "msgstr \"";
  public static final String LINE_TERMINATOR_STRING = "\n";
  public static final String COMMENT_FLAG_STRING = "# ";
  public static final String QUOTE_STRING = "\"";
  public static final String ROWTRL_MESSAGE_STRING = "RowTrl: ";
  
  private String rowIdRow;
  private String messageId;
  private String messageString;
  private String comments;
  private String column;
  private boolean rowTrl;
  private int columnIndex;
  private POFileObject poFileObject;
  
  public String getRowIdRow() { return rowIdRow; }
  public void setRowIdRow(String rowString) { rowIdRow = rowString; }
  
  public String getMessageId() { return messageId; }
  public void setMessageId(String msgIdRow) { messageId = msgIdRow; }
  
  public String getMessageString() { return messageString; }
  public void setMessageString(String msgStrRow) { messageString = msgStrRow; }
  
  public String getCommentString() { return comments; }
  public void setComments(String comment) { comments = comment; }
  
  public String getColumn() { return column; }
  public void setColumn(String column) { this.column = column; }
  
  public int getColumnIndex() { return columnIndex; }
  public void setColumnIndex(int columnIndex) { this.columnIndex = columnIndex; }
  
  
  public POFileObject getPOFileObject() { return poFileObject; }
  public void setPOFileObject(POFileObject poFile) { poFileObject = poFile; }
  
  public boolean getRowTrl() { return rowTrl; }
  public void setRowTrl(String temp) {
    boolean result = true;
    if (temp.toUpperCase().equals("N") || temp.toUpperCase().equals("NO") || temp.toUpperCase().equals("FALSE")) {
    	result = false;
    }
    rowTrl = result;
  }
  
  public void setRowTrl(boolean trl) {
	rowTrl = trl;
  }

  public int compare(Message o1, Message o2) {
    Integer thisInt = Integer.valueOf(o1.getRowIdRow());
    Integer compareInt = Integer.valueOf(o2.getRowIdRow());
    int result = thisInt.compareTo(compareInt);
    return result;
  }
  
  
}
