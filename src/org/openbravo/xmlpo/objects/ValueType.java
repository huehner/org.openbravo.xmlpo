/* 
 * Copyright (C) 2001-2008 Openbravo SL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.objects;

public class ValueType {
  
  private String name;
  private int intValue;
  
  public ValueType() {}
  
  public ValueType(int e, String name){
    this.intValue = e;
    this.name = name;
  }
  
  public String getName() { return name; }
  public void setName(String nameStr) { name = nameStr; }
  
  public int getIntValue() { return intValue; }
  public void setIntValue(int value) { intValue = value; }
}
