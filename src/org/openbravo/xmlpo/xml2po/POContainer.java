/* 
 * Copyright headissue GmbH, Jens Wilke, 2007 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/


package org.openbravo.xmlpo.xml2po;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.openbravo.xmlpo.objects.FileObject;
import org.openbravo.xmlpo.objects.ValueType;
import org.openbravo.xmlpo.utils.FileContainer;
import org.openbravo.xmlpo.utils.POFileBuilder;


public class POContainer implements FileContainer{

  private static Logger log4j = Logger.getLogger(POContainer.class);
  
  private Collection<FileObject> fileObjects;
  private String outputFolder;
  private boolean ignoreTrl;
  
  /**
   * Instantiate the new container and create a new FileObj Collection
   * 
   */
  public POContainer() {
    fileObjects = new ArrayList<FileObject>();
  }
  
  public POContainer(boolean ignoreMatchingMsgStr) {
    this();
    ignoreTrl = ignoreMatchingMsgStr;
  }
  
  /** 
   * Add a FileObj to be managed within the container
   * 
   * @param fileObj
   */
	public void addFileObject(FileObject fileObj) {
		fileObjects.add(fileObj);
	}

	/**
	 * Retrieve the collection of FileObj objects stored in the container
	 * 
	 * @return FileObject collection
	 */
	public Collection<FileObject> getFileObjects() { return fileObjects; }

	/**
	 * Used to iterate over the FileObj's within the container and generate the output files. 
	 * Enables the use of input folders for file selection
	 * @throws IOException
	 */
	public void writePOs() throws IOException {
    log4j.debug("POContainer.writePOs()");
    for (Iterator<FileObject> iterator = fileObjects.iterator(); iterator.hasNext();) {
      FileObject fileObject = (FileObject) iterator.next( );
      POFileBuilder builder = new POFileBuilder(fileObject);
      if (ignoreTrl) builder.setIgnoreNonTrl(ignoreTrl);
      Collection<ValueType> types = fileObject.getValueTypes();
      log4j.debug("valuetypes set inside writePOs(): " + types.size());
      for (Iterator<ValueType> iterator2 = types.iterator(); iterator2.hasNext();) {
        ValueType type = (ValueType) iterator2.next();
        String outputString = builder.generateFileContent(type);
        
        // write file output
        String file = outputFolder + builder.getFileName();
        log4j.info("Outputting data to: " + file);
        FileWriter fileWriter = new FileWriter(file);
        fileWriter.write(outputString);
        fileWriter.close();
      }
    }
  }
	
	public void setOutputFolder(String folder) { outputFolder = folder; }
}
