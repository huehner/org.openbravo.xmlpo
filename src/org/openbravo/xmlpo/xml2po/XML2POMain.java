/* 
 * Copyright headissue GmbH, Jens Wilke, 2007 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/


package org.openbravo.xmlpo.xml2po;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;

import org.apache.log4j.Logger;
import org.openbravo.xmlpo.exceptions.InvalidFolderException;
import org.openbravo.xmlpo.objects.FileObject;
import org.openbravo.xmlpo.utils.ArgumentValidator;
import org.openbravo.xmlpo.utils.FileContainer;
import org.openbravo.xmlpo.utils.FileHandler;
import org.openbravo.xmlpo.utils.ParserUtils;
import org.xml.sax.SAXException;


/**
 * 
 * See the <a href="http://wiki.openbravo.com/wiki/index.php/Openbravo2po">Wiki Documentation</a>.<br>
 * </br> 
 * 
 * Execution of this class will transform an XML file to a PO (Portable Object) file
 *
 */
public class XML2POMain {
  
  private static Logger log4j = Logger.getLogger(XML2POMain.class);
  
  private static String inputFolder = "";
  private static String outputFolder = "";
  private static boolean ignoreMatchingMsgStr = false;
  private static File[] files;

  /**
   * Main class used to generate a PO (Portable Object) file from an exported XML file. If no arguments are  
   * received then an error is returned with message requesting arguments.
   * 
   * @param args
   * @throws Exception
   */
	public static void main(String[] args) throws Exception {
		if (args.length<1) {
		  throw new Exception("arguments required.");
		} else {
		  parseArgs(args);
		  XML2POMain xml2PoMain = new XML2POMain();
		  for (int i = 0; i < files.length; i++) {
		    log4j.debug("Preparing file: " + files[i]);
		    if (files[i].getName().endsWith(".xml")) {
		      String isValid = ArgumentValidator.validateFile(ArgumentValidator.XML_FILE, new File(files[i].getAbsolutePath()));
          if (isValid.equals("")) {
            xml2PoMain.xml2po(files[i]);
          } else if (!isValid.equals("") && !isValid.equals(ArgumentValidator.IGNORE_FILE_STRING)) {
            throw new InvalidFolderException(isValid);
          }
        }
		  }
		  log4j.info("Process completed\n");
		}
	}
	
	/**
	 * Use a new instance of a SAXParser to parse the input file
	 * 
	 * @param fileName
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	private void xml2po(File file) throws ParserConfigurationException, SAXException, IOException {
	    SAXParser saxParser = ParserUtils.instantiateParser();
	    log4j.debug("SAXParser instantiated: " + "ignoreMatchingMsgStr=" + ignoreMatchingMsgStr);
	    FileObject fileObj = new FileObject();
	    FileContainer container = new POContainer(ignoreMatchingMsgStr);
	    outputFolder = validateFolder(outputFolder, file);
	    container.setOutputFolder(outputFolder);
	    log4j.info("InputFolder: " + inputFolder);
	    log4j.info("Output folder: " + outputFolder);
	    fileObj.setContainer(container);
	    FileHandler handler = new FileHandler(fileObj);
	    log4j.info("fileName: " + file.getAbsolutePath());
	    saxParser.parse(file, handler);
	    log4j.debug("Parsing file completed: ");
	    fileObj = handler.getFileObj();
	    log4j.debug("fileObj updated");
	    fileObj.getContainer().writePOs();
	    log4j.debug("Writing completed");
	}
	
	/**
	 * Parse the argument parameters and set appropriate variables. 
	 * 
	 * @param args
	 * @throws InvalidFolderException 
	 */
	private static void parseArgs(String[] args) throws InvalidFolderException {
		if (args[0] != null && !(args[0].equals(""))) {
	      outputFolder = args[0];
	      if (!outputFolder.endsWith("/")) outputFolder = outputFolder + "/";
	      log4j.debug("output folder: " + outputFolder);
	      String isValid = ArgumentValidator.validateFolder(ArgumentValidator.OUTPUT_FOLDER, outputFolder);
	      if (!isValid.equals("")) throw new InvalidFolderException(isValid);
	    }
	    if (args[1] != null && !(args[1].equals(""))) {
	      inputFolder = args[1];
	      if (!inputFolder.endsWith("/")) inputFolder = inputFolder + "/";
	      log4j.debug("inputFolder: " + inputFolder);
	      String isValid = ArgumentValidator.validateFolder(ArgumentValidator.INPUT_FOLDER, inputFolder);
	      if (!isValid.equals("")) throw new InvalidFolderException(isValid);
	    }
	    ignoreMatchingMsgStr = Boolean.parseBoolean(args[2]);
	    if (args.length < 4) {
	      if (!inputFolder.equals("")) {
	        File file = new File(inputFolder);
	        files = file.listFiles();
	        log4j.debug("files being generated from input folder: " + files.length);
	      }
	    } else {
	      files = new File[args.length - 3];
	      for (int i = 3; i < args.length; i++) {
	        log4j.debug("inputFile: " + inputFolder + args[i]);
	        files[i-3] = new File(inputFolder + args[i]);
	      }
	    }
	}
  
  private String validateFolder(String folder, File fileName) {
    String result = new String();
    if (folder == null || folder.equals("")) {
      String fileFolder = fileName.getAbsolutePath().substring(0, fileName.getAbsolutePath().lastIndexOf("/") + 1);
      result = fileFolder;
    } else {
      result = folder;
    }
    return result;
  }
	
}
