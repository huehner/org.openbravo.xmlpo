/* 
 * Copyright (C) 2001-2008 Openbravo SL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.merge;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.File;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestPOMerge {
  
  private File primaryTestFilePO;
  private File secondTestFilePO;
  private File firstSampleFile;
  private File secondSampleFile;
  
  private String kelNameFile = "tests/resources/liveSample/output/AD_TASK_Name_it_IT_Kelyon.po";
  private String kelDescriptionFile = "tests/resources/liveSample/output/AD_TASK_Description_it_IT_Kelyon.po";
  private String kelHelpFile = "tests/resources/liveSample/output/AD_TASK_Help_it_IT_Kelyon.po";
  private String CBTNameFile = "tests/resources/liveSample/output/AD_TASK_Name_it_IT_CBT.po";
  private String CBTDescriptionFile = "tests/resources/liveSample/output/AD_TASK_Description_it_IT_CBT.po";
  private String CBTHelpFile = "tests/resources/liveSample/output/AD_TASK_Help_it_IT_CBT.po";
  
  @Before
  public void setUp() throws Exception {
    primaryTestFilePO = new File("tests/resources/merge/first/AD_TASK_Name_it_IT.po");
    secondTestFilePO = new File("tests/resources/merge/second/AD_TASK_Name_it_IT.po");
    firstSampleFile = new File("tests/resources/merge/first/SampleMergeFile1.po");
    secondSampleFile = new File("tests/resources/merge/second/SampleMergeFile2.po");
    
    firstSampleFile = new File("tests/resources/merge/first/SampleMergeFile1.po");
    secondSampleFile = new File("tests/resources/merge/second/SampleMergeFile2.po");
  }
  
  @After
  public void tearDown() throws Exception {
    firstSampleFile = null;
    secondSampleFile = null;
  }
  
  @Test
  public final void testMain() {
    try {
      POMerge.main(new String[] {"", primaryTestFilePO.getAbsolutePath(), secondTestFilePO.getAbsolutePath()});
    } catch (Exception e) {
      e.printStackTrace();
      fail("Valid arguments passed: " + e.getMessage());
    }
    
  }
  
  @Test
  public final void testMainGoodSamples() {
    try {
      POMerge.main(new String[] {"", firstSampleFile.getAbsolutePath(), secondSampleFile.getAbsolutePath()});
    } catch (Exception e) {
      e.printStackTrace();
      fail("Valid arguments passed: " + e.getMessage());
    }
  }
  
  @Test
  public final void testLiveSamplesName() {
    try {
      POMerge.main(new String[] {"", kelNameFile, CBTNameFile});
    } catch (Exception e) {
      e.printStackTrace();
      fail("Valid arguments passed: " + e.getMessage());
    }
  }
  
  @Test
  public final void testLiveSamplesDescription() {
    try {
      POMerge.main(new String[] {"", kelDescriptionFile, CBTDescriptionFile});
    } catch (Exception e) {
      e.printStackTrace();
      fail("Valid arguments passed: " + e.getMessage());
    }
  }
  
  @Test
  public final void testLiveSamplesHelp() {
    try {
      POMerge.main(new String[] {"", kelHelpFile, CBTHelpFile});
    } catch (Exception e) {
      e.printStackTrace();
      fail("Valid arguments passed: " + e.getMessage());
    }
  }
  
  @Test
  public final void testInvalidFolderExceptionHandledOutput() {
    try {
      POMerge.main(new String[] {"invalidOutputFolder", firstSampleFile.getAbsolutePath(), secondSampleFile.getAbsolutePath()});
      fail("Exception not caught.");
    } catch (Exception e) {
      assertEquals("Exception not as expected.", "Output folder (invalidOutputFolder/) does not exist.", e.getMessage());
    }
  }
  
  @Test
  public final void testInvalidFolderExceptionHandledFileNotExists() {
    try {
      POMerge.main(new String[] {"", "invalidFileName", secondSampleFile.getAbsolutePath()});
      fail("Exception not caught.");
    } catch (Exception e) {
      assertEquals("Exception not as expected.", "File (invalidFileName) does not exist.", e.getMessage());
    }
  }
  
  @Test
  public final void testInvalidFolderExceptionHandledNotDirectory() {
    try {
      POMerge.main(new String[] {firstSampleFile.getAbsolutePath(), secondSampleFile.getAbsolutePath()});
      fail("Exception not caught.");
    } catch (Exception e) {
      assertEquals("Exception not as expected.", "Output folder (" + firstSampleFile.getAbsolutePath() + "/) is not a folder.", e.getMessage());
    }
  }}
