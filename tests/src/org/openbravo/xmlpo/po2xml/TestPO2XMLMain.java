/* 
 * Copyright (C) 2001-2008 Openbravo SL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.po2xml;


import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openbravo.xmlpo.po2xml.PO2XMLMain;

public class TestPO2XMLMain {
  
  private String inputFolder;
  private String outputFolder;
  private String fileName;
  
  @Before
  public void setUp() throws Exception {
    inputFolder = "tests/resources/PO2XML/input";
    outputFolder = "tests/resources/PO2XML/output";
    fileName = "AD_TASK_Name_en_US.po";
  }
  
  @After
  public void tearDown() throws Exception {
  }
  
  @Test
  public void testMainArgsFolders() {
    try {
      PO2XMLMain.main(new String[] { outputFolder, inputFolder });
    } catch (Exception e) {
      fail(e.getMessage());
    }
  }
  
  @Test
  public void testMainArgsFoldersTwoFiles() {
    String file2 = "AD_TASK_Description_en_US.po";
    try {
      PO2XMLMain.main(new String[] { outputFolder, inputFolder,  fileName, file2});
    } catch (Exception e) {
      fail(e.getMessage());
    }
  }
  
  @Test
  public void testMainArgsFolderOnly() {
    try {
      PO2XMLMain.main(new String[] { outputFolder, inputFolder});
    } catch (Exception e) {
      fail(e.getMessage());
    }
  }
  
  @Test
  public void testOutputFolderExceptionHandledOutputFolder() {
    try {
      PO2XMLMain.main(new String[] { "testFolderName", inputFolder});
      fail();
    } catch (Exception e) {
      assertEquals("Output folder (testFolderName/) does not exist.", e.getMessage());
    }
  }
  
  @Test
  public void testOutputFolderExceptionHandledInputFolder() {
    try {
      PO2XMLMain.main(new String[] { outputFolder, "inputFolder"});
      fail();
    } catch (Exception e) {
      assertEquals("Input folder (inputFolder/) does not exist.", e.getMessage());
    }
  }
  
}
