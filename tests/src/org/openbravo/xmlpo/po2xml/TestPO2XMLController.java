/* 
 * Copyright (C) 2001-2008 Openbravo SL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.po2xml;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.File;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openbravo.xmlpo.exceptions.InvalidMergeException;
import org.openbravo.xmlpo.objects.POFileObject;


public class TestPO2XMLController {
  
  private static final Logger log4j = Logger.getLogger(TestPO2XMLController.class);
  
  private PO2XMLController controller;
  private File testFileNameColumn = new File("tests/resources/PO2XML/input/AD_TASK_Name_en_US.po");
  private File testFileDescColumn = new File("tests/resources/PO2XML/input/AD_TASK_Description_en_US.po");
  
  @Before
  public void setUp() throws Exception {
    controller = new PO2XMLController();
  }
  
  @After
  public void tearDown() throws Exception {
    controller = null;
  }
  
  @Test
  public void testReadFilesOutputXML() {
    log4j.debug("testReadFilesMultipleFilesSameTable()");
    controller.addFile(testFileNameColumn);
    log4j.debug("testReadFilesMultipleFilesSameTable() - adding second file");
    controller.addFile(testFileDescColumn);
    try {
      controller.readFiles();
      controller.setOutputFolder("tests/resources/PO2XML/output/");
      controller.buildFileObjects();
      controller.buildXMLFiles();
    } catch (InvalidMergeException e) {
      fail(e.getMessage());
    }
    assertEquals(1, controller.getPOFilesObjects().size());
    
  }
  
  @Test
  public void testReadFilesMultipleFilesSameTable() {
    log4j.debug("testReadFilesMultipleFilesSameTable()");
    controller.addFile(testFileNameColumn);
    log4j.debug("testReadFilesMultipleFilesSameTable() - adding second file");
    controller.addFile(testFileDescColumn);
    try {
      controller.readFiles();
    } catch (InvalidMergeException e) {
      fail(e.getMessage());
    }
    assertEquals(1, controller.getPOFilesObjects().size());
    for (Iterator<POFileObject> iterator = controller.getPOFilesObjects().iterator(); iterator.hasNext();) {
      POFileObject poFileObj = (POFileObject) iterator.next();
      assertEquals(16, poFileObj.getMessages().size());
    }
  }
  
  @Test
  public void readFilesBasicSingleFile() {
    controller.addFile(testFileNameColumn);
    try {
      controller.readFiles();
    } catch (InvalidMergeException e) {
      fail(e.getMessage());
    }
    assertEquals(1, controller.getPOFilesObjects().size());
    for (Iterator<POFileObject> iterator = controller.getPOFilesObjects().iterator(); iterator.hasNext();) {
      POFileObject poFile = (POFileObject) iterator.next();
      assertEquals("AD_TASK", poFile.getTableName());
      assertEquals("en_US", poFile.getLanguage());
      assertEquals("Name", poFile.getColumnName());
      assertEquals("AD_TASK_Name_en_US.po", poFile.getFileName());
      assertEquals("Version not as expected.", "2.40", poFile.getVersion());
      assertEquals(8, poFile.getMessages().size());
    }
  }
  
  @Test
  public void testAddFile() {
    controller.addFile(testFileNameColumn);
    assertEquals(1, controller.getFiles().size()); 
    controller.addFile(testFileNameColumn);
    assertEquals(2, controller.getFiles().size());
    controller = new PO2XMLController();
    assertEquals(0, controller.getFiles().size());
    
  }
  
  @Test
  public void testGetFiles() {
    controller.addFile(testFileNameColumn);
    assertEquals(1, controller.getFiles().size()); 
  }
}
