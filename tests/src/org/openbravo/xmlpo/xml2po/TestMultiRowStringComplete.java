package org.openbravo.xmlpo.xml2po;


import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openbravo.xmlpo.po2xml.PO2XMLMain;
import org.openbravo.xmlpo.xml2po.XML2POMain;


public class TestMultiRowStringComplete {
  
  private String outputFolderString;
  private String inputFolder;
  private String fileName;
  private String poFileName;
  
  @Before
  public void setUp() throws Exception {
    inputFolder = "tests/resources/multipleRowStringsTest/input/";
    outputFolderString = "tests/resources/multipleRowStringsTest/output/";
    fileName = "AD_TASK_TRL_en_US.xml";
    poFileName = "AD_TASK_Name_en_US.po";
  }
  
  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testMainWithValidFolderStrings() {
    try {
      XML2POMain.main(new String[] { outputFolderString, inputFolder, "false", fileName });
    } catch (Exception e) {
      fail("valid argument passed." + e.getMessage());
    }
  }
  
  @Test
  public void testMainArgsFoldersTwoFiles() {
    try {
      PO2XMLMain.main(new String[] { outputFolderString, inputFolder,  poFileName});
    } catch (Exception e) {
      fail(e.getMessage());
    }
  }
}
