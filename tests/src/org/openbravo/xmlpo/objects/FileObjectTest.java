/* 
 * Copyright (C) 2001-2008 Openbravo SL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.objects;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class FileObjectTest {
  
  private FileObject object;
  private FileObject defaultObject;
  private ValueType type;
  
  @Before
  public void setUp() throws Exception {
    object = new FileObject();
    
    defaultObject = new FileObject();
    defaultObject.setLanguage("en_US");
    defaultObject.setTableName("AD_TASK");
    
    type = new ValueType();
    type.setIntValue(1);
    type.setName("name");
    
  }
  
  @After
  public void tearDown() throws Exception {
    object = null;
    defaultObject = null;
    type = null;
  }
  
  @Test
  public void testGetLanguage() {
    assertEquals("en_US", defaultObject.getLanguage());
  }
  
  @Test
  public void testSetLanguage() {
    object.setLanguage("en_US");
    assertEquals("en_US", object.getLanguage());
  }
  
  @Test
  public void testGetTableName() {
    assertEquals("AD_TASK", defaultObject.getTableName());
  }
  
  @Test
  public void testSetTableName() {
    object.setTableName("TESTTABLE");
    assertEquals("TESTTABLE", object.getTableName());
  }
  
  @Test
  public void testAddValueType() {
    ValueType type1 = new ValueType();
    type1.setName("name");
    object.addValueType(type1);
    
    assertEquals(1, object.getValueTypes().size());
  }
  
  @Test
  public void testVersionAttribute() {
    String testVersionString = "testVersionNumber";
    object.setVersion(testVersionString);
    
    FileObject newObj = object;
    assertEquals("", testVersionString, newObj.getVersion());
  }
  
}
