/* 
 * Copyright (C) 2001-2008 Openbravo SL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.objects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openbravo.xmlpo.objects.ValueObject;
import org.openbravo.xmlpo.objects.ValueType;

public class ValueObjectTest {

  private ValueObject object;
  private ValueObject defaultObject;
  private ValueType type;
  
  @Before
  public void setUp() throws Exception {
    object = new ValueObject();
    
    defaultObject = new ValueObject();
    defaultObject.setColumn("column");
    defaultObject.setOriginal("original");
    defaultObject.setValue("value");
    
    type = new ValueType(2, "Name2Type");
    defaultObject.setType(type);
  }
  
  @After
  public void tearDown() throws Exception {
    object = null;
    defaultObject = null;
    type = null;
  }
  
  @Test
  public void testGetColumn() {
    assertEquals("column", defaultObject.getColumn());
  }
  
  @Test
  public void testSetColumn() {
    object.setColumn("columnTest");
    assertEquals("columnTest", object.getColumn());
  }
  
  @Test
  public void testGetOriginal() {
    assertEquals("original", defaultObject.getOriginal());
  }
  
  @Test
  public void testSetOriginal() {
    object.setOriginal("originaltest");
    assertEquals("originaltest", object.getOriginal());
  }
  
  @Test
  public void testGetValue() {
    assertEquals("value", defaultObject.getValue());
  }
  
  @Test
  public void testSetValue() {
    object.setValue("valueTest");
    assertEquals("valueTest", object.getValue());
  }
  
  @Test
  public void testGetType() {
    assertEquals(2, defaultObject.getType().getIntValue());
    assertEquals("Name2Type", defaultObject.getType().getName());
  }
  
  @Test
  public void testSetType() {
    object.setType(new ValueType(1, "nameType1"));
    assertNotNull(object.getType());
    assertEquals(1, object.getType().getIntValue());
    assertEquals("nameType1", object.getType().getName());
  }
  
  @Test
  public void testValueObjectCompare() {
    ValueObject obj1 = new ValueObject();
    ValueType type1 = new ValueType(1, "Name");
    obj1.setType(type1);
    obj1.setColumn("Name");
    obj1.setOriginal("Original");
    obj1.setValue("Value");
    
    ValueObject obj2 = new ValueObject();
    ValueType type2 = new ValueType(2, "Description");
    obj2.setType(type2);
    obj2.setColumn("Description");
    obj2.setOriginal("Original");
    obj2.setValue("Value");
    
    ValueObject obj3 = new ValueObject();
    ValueType type3 = new ValueType(3, "Help");
    obj3.setType(type3);
    obj3.setColumn("Help");
    obj3.setOriginal("Original");
    obj3.setValue("Value");
    
    assertEquals("Comparison result incorrect, obj1:obj2.", -1, new ValueObject().compare(obj1, obj2));
    assertEquals("Comparison result incorrect, obj1:obj2.", 1, new ValueObject().compare(obj2, obj1));
    assertEquals("Comparison result incorrect, obj1:obj2.", -1, new ValueObject().compare(obj2, obj3));
  }
  
}
