/* 
 * Copyright (C) 2001-2008 Openbravo SL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.objects;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openbravo.xmlpo.objects.ValueType;

public class ValueTypeTest {
  
  private ValueType valueType;
  private ValueType defaultValueType;
  
  @Before
  public void setUp() throws Exception {
    valueType = new ValueType();
    
    defaultValueType = new ValueType();
    defaultValueType.setIntValue(3);
    defaultValueType.setName("name");
  }
  
  @After
  public void tearDown() throws Exception {
    valueType = null;
    defaultValueType = null;
  }
  
  @Test
  public void testGetName() {
    assertEquals("name", defaultValueType.getName());
  }
  
  @Test
  public void testSetName() {
    valueType.setName("testName");
    assertEquals("testName", valueType.getName());
  }
  
  @Test
  public void testGetIntValue() {
    assertEquals(3, defaultValueType.getIntValue());
  }
  
  @Test
  public void testSetIntValue() {
    valueType.setIntValue(2);
    assertEquals(2, valueType.getIntValue());
  }
  
}
