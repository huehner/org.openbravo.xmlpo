package org.openbravo.xmlpo.utils;


import static org.junit.Assert.*;

import java.io.File;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestArgumentValidator {
  
  @Before
  public void setUp() throws Exception {
  }
  
  @After
  public void tearDown() throws Exception {
  }
  
  @Test
  public void testValidateFolder() {
    //TODO - create test case
    //test folder doesn't exist
    String argType = ArgumentValidator.OUTPUT_FOLDER;
    String folderName = "invalidFolderName";
    
    String result = ArgumentValidator.validateFolder(argType, folderName);
    assertEquals("validation result not as expected.", "Output folder (invalidFolderName) does not exist.", result);
    
    folderName = "tests/resources/PO2XML/";
    result = ArgumentValidator.validateFolder(argType, folderName);
    assertEquals("Valid folder passed. Should not fail.", "", result);
    //test folder is not a folder
    
    folderName = "README";
    result = ArgumentValidator.validateFolder(argType, folderName);
    assertEquals("validation result not as expected.", "Output folder (README) is not a folder.", result);
    
    folderName = "tests/resources/PO2XML/";
    result = ArgumentValidator.validateFolder(argType, folderName);
    assertEquals("Valid folder passed. Should not fail.", "", result);
  }
  
  @Test
  public void testValidateFile() {
    //TODO - create test case
    //test file does not exist
    String argType = ArgumentValidator.XML_FILE;
    File filename = new File("tests/resources/invalidFileName");
    String result = ArgumentValidator.validateFile(argType, filename);
    assertEquals("Invalid file passed, result not as expected.", "XML file (tests/resources/invalidFileName) does not exist.", result);
    
    filename = new File("tests/resources/AD_TASK_TRL_en_US.xml");
    result = ArgumentValidator.validateFile(argType, filename);
    assertEquals("Valid file passed, should not fail.", "", result);
    
    //test file is a directory
    filename = new File("tests/resources/PO2XML/");
    result = ArgumentValidator.validateFile(argType, filename);
    assertEquals("Folder passed, result not as expected.", "XML file (tests/resources/PO2XML) is a folder, should be a file.", result);
    
    filename = new File("tests/resources/CONTRIBUTORS_fr_FR.xml");
    result = ArgumentValidator.validateFile(argType, filename);
    assertEquals("Folder passed, result not as expected.", "ignore", result);
  }
  
}
