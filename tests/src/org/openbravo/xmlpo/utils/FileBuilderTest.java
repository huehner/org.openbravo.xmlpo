/* 
 * Copyright (C) 2001-2008 Openbravo SL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
*/

package org.openbravo.xmlpo.utils;

import static org.junit.Assert.*;

import java.util.Collection;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openbravo.xmlpo.objects.FileObject;
import org.openbravo.xmlpo.objects.Message;
import org.openbravo.xmlpo.objects.RowObject;
import org.openbravo.xmlpo.objects.ValueObject;
import org.openbravo.xmlpo.objects.ValueType;
import org.openbravo.xmlpo.utils.MessageBuilder;
import org.openbravo.xmlpo.utils.POFileBuilder;


public class FileBuilderTest {
  
  private static final Logger log4j = Logger.getLogger(FileBuilderTest.class);
  
  private static final String FILENAME = "AD_TASK_TRL_name_en_US.po";
  
  private FileObject defaultFileObj;
  private ValueType valueType;
  private String defaultHeader;
  private FileObject populatedFile;
  private String populatedContent;
  
  @Before
  public void setUp() throws Exception {
    defaultFileObj = new FileObject();
    defaultFileObj.setLanguage("en_US");
    defaultFileObj.setTableName("AD_TASK_TRL");
    
    valueType = new ValueType();
    valueType.setIntValue(1);
    valueType.setName("name");
    
    defaultHeader = buildDefaultHeaderString();
    
    populatedFile = createPopulatedFileObject();
  }
  
  @After
  public void tearDown() throws Exception {
    defaultFileObj = null;
    valueType = null;
    defaultHeader = "";
    populatedFile = null;
    populatedContent = "";
  }
  
  @Test
  public final void testFileBuilder() {
    POFileBuilder builder = new POFileBuilder(defaultFileObj);
    
    FileObject object = builder.getFileObject();
    assertEquals(defaultFileObj.getTableName(), object.getTableName());
    assertEquals(defaultFileObj.getLanguage(), object.getLanguage());
  }
  
  @Test
  public final void testAddMessages() {
    POFileBuilder builder = new POFileBuilder();
    builder.setFileObject(populatedFile);
    builder.setValueType(valueType);
    builder.addMessages();
    
    assertNotNull(builder.getMessages());
    assertEquals(2, builder.getMessages().size());
  }
  
  @Test
  public final void testCreateContentString() {
    POFileBuilder builder = new POFileBuilder();
    String result = builder.getContentString();
    assertEquals("No content found.", result);
  }
  
  @Test
  public final void testGetContentString() {
    POFileBuilder builder = new POFileBuilder();
    builder.setFileObject(populatedFile);
    builder.setValueType(valueType);
    builder.addMessages();
    builder.setContentString();
    
    log4j.debug(builder.getContentString());
    
    assertNotNull(builder.getContentString());
    assertEquals(populatedContent, builder.getContentString());
  }
  
  @Test
  public final void testGetFileNameEmpty() {
    POFileBuilder builder = new POFileBuilder();
    assertEquals("", builder.getFileName());
  }
  
  @Test
  public final void testSetValueType() {
    POFileBuilder builder = new POFileBuilder();
    builder.setValueType(valueType);
    
    ValueType result = builder.getValueType();
    
    assertEquals(valueType.getIntValue(), result.getIntValue());
    assertEquals(valueType.getName(), result.getName());
  }
  
  @Test
  public final void testSetFileName() {
    POFileBuilder builder = new POFileBuilder(defaultFileObj);
    builder.setValueType(valueType);
    builder.setFileName(defaultFileObj.getTableName(), valueType.getName(), defaultFileObj.getLanguage());
    
    assertEquals(FILENAME, builder.getFileName());
  }
  
  @Test
  public final void testGetFileObject() {
    POFileBuilder builder = new POFileBuilder();
    FileObject object = builder.getFileObject();
    
    assertNull(object);
  }
  
  @Test
  public final void testSetFileObject() {
    POFileBuilder builder = new POFileBuilder();
    builder.setFileObject(defaultFileObj);
    FileObject object = builder.getFileObject();
    assertEquals(defaultFileObj.getTableName(), object.getTableName());
    assertEquals(defaultFileObj.getLanguage(), object.getLanguage());
  }
  
  @Test
  public final void testSetHeader() {
    POFileBuilder builder = new POFileBuilder();
    builder.setFileObject(defaultFileObj);
    builder.setValueType(valueType);
    builder.setFileName(defaultFileObj.getTableName(), valueType.getName(), defaultFileObj.getLanguage());
    builder.setHeader(builder.getFileName(), defaultFileObj.getTableName(), valueType.getName(), defaultFileObj.getLanguage(), defaultFileObj.getVersion(), valueType.getIntValue());
    assertNotNull(builder.getHeader());
    assertEquals(defaultHeader, builder.getHeader());
    
  }
  
  @Test
  public final void testGetHeader() {
    POFileBuilder builder = new POFileBuilder();
    String result = builder.getHeader();
    assertEquals("Header not created.", result);
  }
  
  @Test
  public void testCreateMsgWithMultipleValueObjects() {
    POFileBuilder poFileBuilder = new POFileBuilder();
    
    // create the test objects
    RowObject testRowObj = new RowObject();
    testRowObj.setRowId("217");
    testRowObj.setTranslation("Y");
    ValueObject vo1 = createValueObject("Name", "Name Matching", "");
    vo1.setType(new ValueType(1, "name"));
    testRowObj.addValueObject(vo1);
    ValueObject vo2 = createValueObject("Help", "Help Matching", "");
    vo2.setType(new ValueType(3, "help"));
    testRowObj.addValueObject(vo2);
    ValueObject vo3 = createValueObject("Description", "Description Matching vo", "");
    vo3.setType(new ValueType(2, "Description"));
    testRowObj.addValueObject(vo3);
    
    
    RowObject testRO2 = new RowObject();
    testRO2.setRowId("5973945635C6FAB2E040A8C0846648BC");
    testRO2.setTranslation("Y");
    ValueObject vo4 = createValueObject("Name", "Name Matching", "");
    vo4.setType(new ValueType(1, "name"));
    testRO2.addValueObject(vo4);
    ValueObject vo5 = createValueObject("Help", "Help Not Matching but is", "");
    vo5.setType(new ValueType(3, "help"));
    testRO2.addValueObject(vo5);
    ValueObject vo6 = createValueObject("Description", "Description Not Matching", "");
    vo6.setType(new ValueType(2, "Description"));
    testRO2.addValueObject(vo6);
    
    RowObject testRO3 = new RowObject();
    testRO3.setRowId("312");
    testRO3.setTranslation("Y");
    ValueObject vo7 = createValueObject("Name", "Name Not Matching", "");
    vo7.setType(new ValueType(1, "name"));
    testRO3.addValueObject(vo7);
    ValueObject vo8 = createValueObject("Help", "Help not Matching but is", "");
    vo8.setType(new ValueType(3, "help"));
    testRO3.addValueObject(vo8);
    ValueObject vo9 = createValueObject("Description", "Description Matching vo", "");
    vo9.setType(new ValueType(2, "Description"));
    testRO3.addValueObject(vo9);
    
    RowObject testRO4 = new RowObject();
    testRO4.setRowId("1");
    testRO4.setTranslation("Y");
    ValueObject vo10 = createValueObject("Name", "Name Matching", "");
    vo10.setType(new ValueType(1, "name"));
    testRO4.addValueObject(vo10);
    ValueObject vo11 = createValueObject("Help", "Help Not Matching but is", "");
    vo11.setType(new ValueType(3, "help"));
    testRO4.addValueObject(vo11);
    ValueObject vo12 = createValueObject("Description", "Description Not Matching", "");
    vo12.setType(new ValueType(2, "Description"));
    testRO4.addValueObject(vo12);
    
    // build the test messages
    MessageBuilder builder = new MessageBuilder(testRowObj, new ValueType(1, "name"));
    assertEquals(1, builder.getValueType().getIntValue());
    Message result1 = builder.buildMessage();
    
    builder = new MessageBuilder(testRO2, new ValueType(1, "name"));
    assertEquals(1, builder.getValueType().getIntValue());
    Message result2 = builder.buildMessage();
    
    builder = new MessageBuilder(testRO3, new ValueType(1, "name"));
    assertEquals(1, builder.getValueType().getIntValue());
    Message result3 = builder.buildMessage();
    
    // test merge functionality with result1, result2, and result3
    boolean result = poFileBuilder.addMessage(result1);
    assertEquals("testCreateMsgWithMultipleValueObjects() - result1: result not correct.", false, result);
    result = poFileBuilder.addMessage(result2);
    assertEquals("testCreateMsgWithMultipleValueObjects() - result2: result not correct.", true, result);
    result = poFileBuilder.addMessage(result3);
    assertEquals("testCreateMsgWithMultipleValueObjects() - result3: result not correct.", false, result);
    
    Collection<Message> msgs = poFileBuilder.getMessages();
    String rowString = Message.MESSAGE_FLAG_STRING + Message.LINE_TERMINATOR_STRING + Message.COMMENT_FLAG_STRING + "217,311" + Message.LINE_TERMINATOR_STRING;
    for (Iterator<Message> iterator = msgs.iterator(); iterator.hasNext();) {
      Message message = (Message) iterator.next();
      if (message.getMessageId().equals("msgid \"Name Matching\"")) {
        assertEquals("mesg rowId string not as expected: ", rowString, message.getRowIdRow());
      }
    }
    
    builder = new MessageBuilder(testRO4, new ValueType(1, "name"));
    assertEquals(1, builder.getValueType().getIntValue());
    Message result4 = builder.buildMessage();
    
    result = poFileBuilder.addMessage(result4);
    assertEquals("testCreateMsgWithMultipleValueObjects() - result3: result not correct.", true, result);
    assertEquals("testCreateMsgWithMultipleValueObjects(): no of messages not equal.", 2, poFileBuilder.getMessages().size());
    
    msgs = poFileBuilder.getMessages();
    rowString = Message.MESSAGE_FLAG_STRING + Message.LINE_TERMINATOR_STRING + Message.COMMENT_FLAG_STRING + "1,217,5973945635C6FAB2E040A8C0846648BC" + Message.LINE_TERMINATOR_STRING;
    for (Iterator<Message> iterator = msgs.iterator(); iterator.hasNext();) {
      Message message = (Message) iterator.next();
      if (message.getMessageId().equals("msgid \"Name Matching\"")) {
        assertEquals("mesg rowId string not as expected: ", rowString, message.getRowIdRow());
      }
    }
  }
  
  private ValueObject createValueObject(String column, String original, String value) {
    ValueObject valueObj = new ValueObject();
    valueObj.setColumn(column);
    valueObj.setOriginal(original);
    valueObj.setValue(value);
    return valueObj;
  }
  
  private String buildDefaultHeaderString() {
    StringBuffer buffer = new StringBuffer();
    buffer.append("msgid \"\"\n");
    buffer.append("msgstr \"\"\n");
    buffer.append("\"Content-Type: text/plain; charset=UTF-8\\n\"\n");
    buffer.append("\"Content-Transfer-Encoding: 8bit\\n\"\n");
    // w.write("\"Mime-Version: 1.0\"\n");
    buffer.append("\"Last-Translator: \\n\"\n");
    buffer.append("\"PO-Revision-Date: \\n\"\n");
    buffer.append("\"Project-Id-Version: " + FILENAME + "\\n\"\n");
    buffer.append("\"Language-Team:  \\n\"\n");
    buffer.append("\"X-Generator: org.openbravo.xmlpo.xml2po\\n\"\n");
    buffer.append("\"X-Openbravo-Table: " + defaultFileObj.getTableName() + "\\n\"\n");
    buffer.append("\"X-Openbravo-Column: " + valueType.getName() + "\\n\"\n");
    buffer.append("\"X-Openbravo-Index-Column: " + valueType.getIntValue() + "\\n\"\n");
    buffer.append("\"X-Openbravo-Language: " + defaultFileObj.getLanguage() + "\\n\"\n");
    buffer.append("\"X-Openbravo-Version: " + defaultFileObj.getVersion() + "\\n\"\n");
    buffer.append("\"MIME-Version: 1.0\\n\"\n");
    buffer.append("\n");
    return buffer.toString();
  }
  
  private RowObject createRowObject(String rowId, String translation) {
    RowObject object = new RowObject();
    object.setRowId(rowId);
    object.setTranslation(translation);
    return object;
  }
  
  private ValueObject createValueObject(String column, String original, String value, ValueType type) {
    ValueObject object = new ValueObject();
    object.setColumn(column);
    object.setOriginal(original);
    object.setValue(value);
    object.setType(type);
    return object;
  }
  
  private FileObject createPopulatedFileObject() {
    FileObject fileObj = new FileObject();
    fileObj.setLanguage("en_US");
    fileObj.setTableName("AD_TASK_TRL");
    
    RowObject rowObj1 = createRowObject("5973945635C6FAB2E040A8C0846648BC", "Y");
    RowObject rowObj2 = createRowObject("10002", "Y");
    
    ValueObject valueObj1 = createValueObject("name", "name original1Row1", "name value1Row1", new ValueType(1, "name"));
    ValueObject valueObj2 = createValueObject("name", "name original2Row1", "name value2Row1", new ValueType(2, "description"));
    ValueObject valueObj3 = createValueObject("name", "name original3Row1", "name value3Row1", new ValueType(3, "help"));
    rowObj1.addValueObject(valueObj1);
    rowObj1.addValueObject(valueObj2);
    rowObj1.addValueObject(valueObj3);
    
    valueObj1 = createValueObject("name", "name original1Row2", "name value1Row2", new ValueType(1, "name"));
    valueObj2 = createValueObject("name", "name original2Row2", "name value2Row2", new ValueType(2, "description"));
    valueObj3 = createValueObject("name", "name original3Row2", "name value3Row2", new ValueType(3, "help"));
    rowObj2.addValueObject(valueObj1);
    rowObj2.addValueObject(valueObj2);
    rowObj2.addValueObject(valueObj3);
    
    fileObj.addRowObject(rowObj1);
    fileObj.addRowObject(rowObj2);
    
    return fileObj;
  }
  
  @Before
  public void setUpPopulatedContentResult() throws Exception { 
    StringBuffer buffer = new StringBuffer();
    buffer.append("# RowIds:\n");
    buffer.append("# 5973945635C6FAB2E040A8C0846648BC\n");
    buffer.append("# RowTrl: Y\n");
    buffer.append("# Name: name original1Row1 [5973945635C6FAB2E040A8C0846648BC]\n");
    buffer.append("# Description: name original2Row1 [5973945635C6FAB2E040A8C0846648BC]\n");
    buffer.append("# Help: name original3Row1 [5973945635C6FAB2E040A8C0846648BC]\n");
    buffer.append("msgid \"name original1Row1\"\n");
    buffer.append("msgstr \"name value1Row1\"\n\n");
    buffer.append("# RowIds:\n");
    buffer.append("# 10002\n");
    buffer.append("# RowTrl: Y\n");
    buffer.append("# Name: name original1Row2 [10002]\n");
    buffer.append("# Description: name original2Row2 [10002]\n");
    buffer.append("# Help: name original3Row2 [10002]\n");
    buffer.append("msgid \"name original1Row2\"\n");
    buffer.append("msgstr \"name value1Row2\"\n\n");
    populatedContent = buffer.toString();
  }
}
